package fr.umontpellier.iut.exercice4;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BowlingTest {

    @Disabled
    @Test
    public void test_calculer_score_avec_aucune_quille_tombee() {
        Bowling jeu = new Bowling();
        int score = jeu.calculerScore("--------------------");

        assertEquals(0, score);
    }

    @Disabled
    @Test
    public void test_calculer_score_golden_score() {
        Bowling jeu = new Bowling();
        int score = jeu.calculerScore("XXXXXXXXXXXX");

        assertEquals(300, score);
    }
    
    @Disabled
    @Test
    public void test_calculer_score_avec_21_jets_tous_avec_spare() {
        Bowling jeu = new Bowling();
        int score = jeu.calculerScore("4/4/4/4/4/4/4/4/4/4/4");
        assertEquals(140, score);
    }

    @Disabled
    @Test
    public void test_calculer_score_avec_que_des_quilles_tombees() {
        Bowling jeu = new Bowling();
        int score = jeu.calculerScore("12345123451234512345");

        assertEquals(60, score);
    }

    @Disabled
    @Test
    public void test_calculer_score_avec_que_des_quilles_tombees_et_de_ratés() {
        Bowling jeu = new Bowling();
        int score = jeu.calculerScore("9-9-9-9-9-9-9-9-9-9-");

        assertEquals(90, score);
    }
}